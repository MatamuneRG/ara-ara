<!DOCTYPE html>
<html lang="en">
<head>
    <?php require 'extensiones/head.php' ?>
    <link rel="stylesheet" href="estilos/estilos.css">
    <title><Menu></Menu></title>
</head>
<body>
    <div class="menu">
        <form action="trabajador.php" method="post">
            <div class="titulo">
                <h3>Menu principal</h3>
                <hr>   
            </div>
            <div class="opciones">
                <div class="row">
                    <div class="col-md-4">
                        <button type="submit" class="btn btn-block" style="background: red; color: white">Agregar Trabajador</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <?php require 'extensiones/boostrap.php' ?>
</body>
</html>